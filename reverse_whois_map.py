import csv
import json
import os
import csv

from math import log2, ceil
from treelib import Tree, Node
from typing import Dict, List, Tuple, Union
from intervaltree import Interval, IntervalTree
from ipaddress import IPv4Address, IPv6Address, ip_address


FIRST_IPV4_RANGE = ip_address("0.0.0.0")
LAST_IPV4_RANGE = ip_address("255.255.255.255")


def sort_csv_by_first_then_second_column(input_path, output_path):
    with open(input_path, mode='r', newline='') as file:
        reader = csv.reader(file)
        data = list(reader)

    sorted_data = sorted(data, key=lambda row: (int(row[0]), -int(row[1])))
    
    with open(output_path, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(sorted_data)


def sort_key(node: Node):
    return node.data.ip_start


def convert_to_ip_address(ip: str):
    try:
        if isinstance(ip, str):
            ip_int = int(ip)
        else:
            ip_int = ip

        if 0 <= ip_int <= 2**128 - 1:
            return ip_address(ip_int)
        else:
            raise ValueError("The integer is not within the valid range for IP addresses.")

    except ValueError as e:
        print(f"Error: {e}")


def get_ip(ip: Union[IPv4Address, IPv6Address], next: bool):
    if next:
        return ip_address(int(ip) + 1)
    elif ip != FIRST_IPV4_RANGE:
        return ip_address(int(ip) - 1)
    else:
        return ip


class CustomNode:
    def __init__(self, ip_start: Union[IPv4Address, IPv6Address], ip_end: Union[IPv4Address, IPv6Address], is_logical_leaf: bool = False):
        self.ip_start = ip_start
        self.ip_end = ip_end
        self.is_logical_leaf = is_logical_leaf
        self.children = IntervalTree()

        self.__dict__.update({'ip_range': '[{}, {}]'.format(ip_start, ip_end)})

    def get_tag(self):
        return f"[{int(self.ip_start)}, {int(self.ip_end)}]"

    def add_child(self, child_node):
        if child_node.ip_start != child_node.ip_end:
            self.children.add(Interval(child_node.ip_start, child_node.ip_end, child_node))
        
    def __str__(self) -> str:
        return f"[{self.ip_start}, {self.ip_end}]"

    def __eq__(self, other):
        return (self.ip_start, self.ip_end) == (other.ip_start, other.ip_end)

    def __hash__(self):
        return hash((self.ip_start, self.ip_end))

    def __repr__(self):
        return f"[{self.ip_start}, {self.ip_end}]"


def _build_custom_node(ip_range_tag: str) -> CustomNode:
    str_ip_start, str_ip_end = ip_range_tag.replace(" ", "")[1:-1].split(",")
    ip_start = convert_to_ip_address(str_ip_start)
    ip_end = convert_to_ip_address(str_ip_end)

    new_node = CustomNode(ip_start, ip_end)
    return new_node


def build_tree_from_json(json_data, tree: Tree, parent=None):

    if isinstance(json_data, dict):
        for key, value in json_data.items():
            tree.create_node(tag=key, identifier=key, parent=parent, data=_build_custom_node(key))
            if isinstance(value, dict) and 'children' in value:
                build_tree_from_json(value['children'], tree=tree, parent=key)
    elif isinstance(json_data, list):
        for item in json_data:
            build_tree_from_json(item, tree=tree, parent=parent)
    elif isinstance(json_data, str):
        tree.create_node(tag=json_data, identifier=json_data, parent=parent, data=_build_custom_node(json_data))


def create_tree(filename: str, tree: Tree, limit: int = None):
    if os.path.isfile("tree.json"):
        # Create the tree from JSON data
        with open("tree.json", 'r') as file:
            json_data = json.loads(file.read())
            build_tree_from_json(json_data, tree=tree)
            return


    # This node is added as root because treelib doesn't allow more than 1 root in the tree
    # The node range is [0.0.0.0, ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff] covering the entire range of ipv4 and ipv6
    root_ip_start = convert_to_ip_address(0)
    root_ip_end = convert_to_ip_address(2**128 - 1)
    root = CustomNode(root_ip_start, root_ip_end)
    tree.create_node(root.get_tag(), root.get_tag(), data=root)

    with open(filename, 'r') as file:
        reader = csv.reader(file)
        cnt = 0
        for row in reader:
            if limit and cnt == limit:
                break
            cnt += 1

            ip_start = convert_to_ip_address(row[0])
            ip_end = convert_to_ip_address(row[1])

            if int(ip_start) > int(LAST_IPV4_RANGE): # Only ipv4
                continue
            elif ip_start > ip_end:  # If ip_start is greater than ip_end skip
                continue

            new_node = CustomNode(ip_start, ip_end)
            insert_into_tree(root, new_node, tree)


def insert_into_tree(parent: CustomNode, new_node: CustomNode, tree: Tree):
    overlaps = parent.children.overlap(new_node.ip_start, new_node.ip_end)

    # The Interval object doesn't allow a range with the same start and end value.
    # So, It uses the immediately preceding IP, which is likely to belong to the same parent range.
    # Example: ├── [87.193.128.0, 87.193.191.255]
    #                    │   └── [87.193.169.160, 87.193.169.160]
    # When inserting [87.193.169.160, 87.193.169.160] it'll call the overlap function with the range [87.193.169.159, 87.193.169.160] 
    # returning the interval [87.193.128.0, 87.193.191.255] where it belongs.
    # If the ip_start -1 is outside the parent range, it uses the immediately next IP
    # Example: ├── [87.193.128.0, 87.193.191.255]
    #          │   └── [87.193.128.0, 87.193.128.0]
    # When inserting [87.193.128.0, 87.193.128.0] it'll call the overlap function with the range [87.193.128.0, 87.193.128.1]
    # returning the interval [87.193.128.0, 87.193.191.255] where it belongs.
    if new_node.ip_start == new_node.ip_end:
        previous_ip_start = get_ip(new_node.ip_start, False)
        next_ip_end = get_ip(new_node.ip_end, True)
        if int(previous_ip_start) >= int(parent.ip_start) and previous_ip_start != FIRST_IPV4_RANGE:
            overlaps = parent.children.overlap(previous_ip_start, new_node.ip_end)
        elif int(next_ip_end) <= int(parent.ip_end):
            overlaps = parent.children.overlap(new_node.ip_start, next_ip_end)

    if overlaps:
        for interval in overlaps:
            insert_into_tree(interval.data, new_node, tree)
            return
    parent.add_child(new_node)
    tree.create_node(new_node.get_tag(), new_node.get_tag(), parent=parent.get_tag(), data=new_node)


def get_nodes_in_depth(tree: Tree, depth: int):
    nodes: List[str] = []

    for node in tree.all_nodes_itr():
        node_level = tree.level(node.identifier)
        if node_level - 1 == depth:
            nodes.append(node.data.ip_range)
    return nodes


def get_count_nodes_per_level(tree: Tree):
    nodes_count: Dict[str, int] = {}

    for node in tree.all_nodes_itr():
        node_level = tree.level(node.identifier)
        str_node_level = str(node_level-1)
        if node_level != 0 and str_node_level not in nodes_count:
            nodes_count[str_node_level] = 1
        elif node_level != 0:
            nodes_count[str_node_level] += 1
    return nodes_count


def average_ips_and_depth_in_leaf_nodes(tree: Tree) -> Tuple[float, float]:
    total_ips = 0
    total_depth = 0

    leaf_nodes: List[Node] = tree.leaves()
    for node in leaf_nodes:
        total_ips += int(node.data.ip_end) - int(node.data.ip_start) + 1
    
        node_level = tree.level(node.identifier)
        total_depth += node_level - 1 # -1 because the root

    if len(leaf_nodes) > 0:
        return total_ips / len(leaf_nodes), total_depth / len(leaf_nodes)
    else:
        return 0, 0


def list_top_nodes_by_child_count(tree: Tree) -> List[Tuple[int, Node]]:
    is_root = True
    nodes_with_children: List[Tuple[int, Node]] = []

    for node in tree.all_nodes_itr():
        if is_root:
            is_root = False
            continue

        children_count = len(tree.children(node.identifier))
        nodes_with_children.append((children_count, node))

    top_nodes = sorted(nodes_with_children, key=lambda x: x[0], reverse=True)[:5]

    return top_nodes


def average_children_per_node(tree: Tree):
    total_children = 0
    node_count = 0
    is_root = True

    for node in tree.all_nodes_itr():
        if is_root:
            is_root = False
            continue
        
        children = len(tree.children(node.identifier))
        if children > 0:
            total_children += children
            node_count += 1

    if node_count > 0:
        return total_children / node_count
    else:
        return 0


def holes_in_ipv4_space(tree: Tree, is_subtree = False) -> Tuple[int, List[CustomNode]]:
    hole_count = 0
    holes_nodes: set[CustomNode] = set()

    first_ipv4_range = int(FIRST_IPV4_RANGE)
    last_ipv4_range = int(LAST_IPV4_RANGE)
    ranges_to_include = [first_ipv4_range, last_ipv4_range]
    ranges: Dict[int, bool] = {}
    for node in tree.all_nodes_itr():
        node_level = tree.level(node.identifier)

        # Skip root level
        if node_level == 0:
            if is_subtree:
                ranges_to_include = []
                str_ip_start, str_ip_end = node.tag.replace(" ", "")[1:-1].split(",")
                ip_start = convert_to_ip_address(str_ip_start)
                ip_end = convert_to_ip_address(str_ip_end)
                ranges_to_include.append(int(ip_start))
                ranges_to_include.append(int(ip_end))
            continue
        

        str_ip_start, str_ip_end = node.tag.replace(" ", "")[1:-1].split(",")

        ip_start = convert_to_ip_address(str_ip_start)
        ip_end = convert_to_ip_address(str_ip_end)

        children = tree.children(node.identifier)
        if len(children) == 0:
            ranges[int(ip_start)] = False
            ranges[int(ip_end)] = False
        else:
            ranges[int(ip_start)] = True
            ranges[int(ip_end)] = True

        if ip_start != FIRST_IPV4_RANGE:
            prev_ip = int(get_ip(ip_start, next=False))
        next_ip = int(get_ip(ip_end, next=True))
        if ip_start != FIRST_IPV4_RANGE:
            if prev_ip not in ranges:
                ranges[prev_ip] = True
        if next_ip not in ranges:
            ranges[next_ip] = True

    for ipv4_range in ranges_to_include:
        if ipv4_range not in ranges:
            ranges[ipv4_range] = True

    keys = sorted(ranges.keys())
    ip_start = None
    ip_end = None

    for i in range(len(keys)):
        if ip_start is None:
            ip_start = keys[i]
        elif ip_end is None:
            ip_end = keys[i]
        elif ranges[ip_start] and ranges[ip_end]:
            holes_nodes.add(CustomNode(convert_to_ip_address(ip_start), convert_to_ip_address(ip_end), True))
            ranges[ip_start] = False
            ranges[ip_end] = False
            hole_count += 1
            ip_start = ip_end
            ip_end = keys[i]
        else:
            ip_start = ip_end
            ip_end = keys[i]
    if ranges[ip_start] and ranges[ip_end]:
        holes_nodes.add(CustomNode(convert_to_ip_address(ip_start), convert_to_ip_address(ip_end), True))
        hole_count += 1

    return hole_count, list(holes_nodes)


def average_holes_per_node_in_ipv4_space(tree: Tree):
    total_holes = 0
    node_count = 0
    is_root = True

    for node in tree.all_nodes_itr():
        if is_root:
            is_root = False
            continue

        children = len(tree.children(node.identifier))
        if children > 0:
            subtree = tree.subtree(node.identifier)
            holes_count, _ = holes_in_ipv4_space(subtree, is_subtree=True)
            total_holes += holes_count
            node_count += 1

    if node_count > 0:
        return total_holes / node_count
    else:
        return 0 


def _cidr_range_size(ip_start: int, ip_end: int):
    num_addresses = ip_end - ip_start + 1
    cidr_range = 32 - int(ceil(log2(num_addresses)))
    return cidr_range


def cidr_ranges(tree: Tree):
    cidr_counts: Dict[int, int] = {}
    cidr_nodes: Dict[int, List[CustomNode]] = {}
    for node in tree.leaves():
        ip_start = int(node.data.ip_start)
        ip_end = int(node.data.ip_end)
        cidr_size = _cidr_range_size(ip_start, ip_end)
        
        if cidr_size in cidr_counts:
            cidr_counts[cidr_size] += 1
            if len(cidr_nodes[cidr_size]) < 5: # Limit to only 5 of them
                cidr_nodes[cidr_size].append(node.data)
        else:
            cidr_nodes[cidr_size] = [node.data]
            cidr_counts[cidr_size] = 1

    return cidr_counts, cidr_nodes
