import csv
import time
from ipaddress import ip_address
from typing import List, Tuple
from treelib import Tree, Node
from ncls import NCLS

from reverse_whois_map import (CustomNode, average_children_per_node,
                               average_holes_per_node_in_ipv4_space,
                               average_ips_and_depth_in_leaf_nodes,
                               cidr_ranges, convert_to_ip_address, get_count_nodes_per_level,
                               get_nodes_in_depth, holes_in_ipv4_space,
                               list_top_nodes_by_child_count, sort_csv_by_first_then_second_column, sort_key)


FIRST_IPV4_RANGE = ip_address("0.0.0.0")
LAST_IPV4_RANGE = ip_address("255.255.255.255")


def write_ips_to_csv(filename, data: List[Node]):
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)

        for row in data:
            node: CustomNode = row.data
            writer.writerow([node.ip_start, node.ip_end, node.is_logical_leaf])


def load_nodes(filename: str, limit: int = None) -> List[CustomNode]:
    nodes = [CustomNode(FIRST_IPV4_RANGE, LAST_IPV4_RANGE)]  # Root node
    with open(filename, 'r') as file:
        for cnt, row in enumerate(csv.reader(file)):
            if limit and cnt >= limit:
                break
            ip_start, ip_end = convert_to_ip_address(row[0]), convert_to_ip_address(row[1])
            if ip_start > LAST_IPV4_RANGE or ip_start > ip_end:
                continue
            nodes.append(CustomNode(ip_start, ip_end))
    return nodes


def build_ncls_tree(custom_nodes: List[CustomNode]) -> NCLS:
    start_points = []
    end_points = []
    ids = []
    
    for i, node in enumerate(custom_nodes):
        start_points.append(int(node.ip_start))
        end_points.append(int(node.ip_end))
        ids.append(i)
    
    return NCLS(start_points, end_points, ids)


def add_nodes_to_tree(tree: Tree, custom_nodes: List[CustomNode], ncls_tree: NCLS, add_root = False):
    for i, custom_node in enumerate(custom_nodes):
        if add_root and i == 0:  # Root node
            tree.create_node(custom_node.get_tag(), custom_node.get_tag(), data=custom_node)
            continue
        if custom_node.ip_start == FIRST_IPV4_RANGE and custom_node.ip_end == FIRST_IPV4_RANGE:
            ip_end_address = ip_address("0.255.255.255")
            parent_tag = f"[{int(FIRST_IPV4_RANGE)}, {int(ip_end_address)}]"
            tree.create_node(custom_node.get_tag(), custom_node.get_tag(), parent=parent_tag, data=custom_node)
            continue
        elif custom_node.ip_start == LAST_IPV4_RANGE and custom_node.ip_end == LAST_IPV4_RANGE:
            parent_tag = f"[{int(FIRST_IPV4_RANGE)}, {int(LAST_IPV4_RANGE)}]"
            tree.create_node(custom_node.get_tag(), custom_node.get_tag(), parent=parent_tag, data=custom_node)
            continue

        contained = ncls_tree.find_overlap(int(custom_node.ip_start), int(custom_node.ip_end))
        contained_in = [(start, end, id) for start, end, id in contained if start <= int(custom_node.ip_start) and end >= int(custom_node.ip_end)]
        #print(f"\nrange: [{int(custom_node.ip_start)}, {int(custom_node.ip_end)}] [{custom_node.ip_start}, {custom_node.ip_end}]")
        parent_tag = find_parent_tag(contained_in, custom_node)
        #print(parent_tag)
        tree.create_node(custom_node.get_tag(), custom_node.get_tag(), parent=parent_tag, data=custom_node)


def find_parent_tag(contained_in: List[Tuple], custom_node: CustomNode):
    parent_tag = None
    for ip_start, ip_end, idx in contained_in:
        if ip_start != int(custom_node.ip_start) or ip_end != int(custom_node.ip_end):
            #print(f"       [{ip_start}, {ip_end}] [{convert_to_ip_address(ip_start)}, {convert_to_ip_address(ip_end)}]")
            parent_tag = f"[{ip_start}, {ip_end}]"
    return parent_tag


def print_tree_statistics(full_tree: Tree, tree: Tree, holes_nodes: List[CustomNode]):
    tree_depth = full_tree.depth() - 1 # -1 because the first level is the root
    nodes_in_depth = get_nodes_in_depth(full_tree, depth=tree_depth)
    top_nodes = list_top_nodes_by_child_count(full_tree)
    avg_children = average_children_per_node(full_tree)
    average_ips, avg_depth = average_ips_and_depth_in_leaf_nodes(full_tree)
    avg_holes_per_node = average_holes_per_node_in_ipv4_space(tree) # Full tree doesn't have holes

    print("Depth: {}".format(tree_depth))
    print("Roots: {}".format(full_tree.size(level=1)))
    print("Leaves: {}".format(len(full_tree.leaves())))
    print("Node count per depth: {}" .format(get_count_nodes_per_level(full_tree)))
    print("Nodes in depth {}: {}".format(tree_depth, nodes_in_depth))
    print("Average depth of leaves:", avg_depth)
    if len(top_nodes) > 0:
        print("Node with the largest number of children:", top_nodes[0][1].data.ip_range)
        print("Largest number of children for any node:", top_nodes[0][0])
        print("Top 5 nodes with more children:")
        for count, node in top_nodes:
            print("     {}: {}".format(node.data.ip_range, count))
    print("Average number of children per node with children:", avg_children)
    print(f"Average number of IPs per leaf node: {average_ips}")
    print("Holes in IPv4 space: {}".format(len(holes_nodes)))
    print("Average number of holes per node with children:", avg_holes_per_node)

    print("\n")
    cidr_counts, cird_nodes = cidr_ranges(full_tree)
    sorted_cidr_sizes = sorted(cidr_counts.keys())
    for cidr in sorted_cidr_sizes:
        print(f"/{cidr}: {cidr_counts[cidr]} leaves")
    print("\n")
    for cidr in sorted_cidr_sizes:
        print(f"/{cidr}: {cird_nodes[cidr]}...")


def main(sort_csv = False, print_statistics = False, print_tree = False, save_tree = False):
    input_csv_path = 'ip_whois_map.csv'
    output_csv_path = 'ip_whois_map_sorted.csv'
    if sort_csv:
        sort_csv_by_first_then_second_column(input_csv_path, output_csv_path)

    custom_nodes = load_nodes(output_csv_path)
    ncls_tree = build_ncls_tree(custom_nodes)

    tree = Tree()
    add_nodes_to_tree(tree, custom_nodes, ncls_tree, True)

    full_tree = Tree(tree)
    _, holes_nodes = holes_in_ipv4_space(tree)
    add_nodes_to_tree(full_tree, holes_nodes, ncls_tree)

    if print_tree:
        print(tree.show(stdout=False, data_property="ip_range", key=sort_key))
        print(full_tree.show(stdout=False, data_property="ip_range", key=sort_key))

    if save_tree:
        full_tree.save2file('full_tree.txt', data_property="ip_range", key=sort_key) 
        write_ips_to_csv('ip_ranges.csv', full_tree.leaves())
    
    if print_statistics:
        print_tree_statistics(full_tree, tree, holes_nodes)

if __name__ == "__main__":
    start_time = time.time()
    main(sort_csv=False, print_statistics=True, print_tree=False, save_tree=False)
    print(f"Total time elapsed: {time.time() - start_time:.4f} seconds")
